<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MotoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=MotoRepository::class)
 */
class Moto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $cylinderSize;

    /**
     * @ORM\OneToOne(targetEntity=model::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $model;

    /**
     * @ORM\OneToOne(targetEntity=motoType::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $motoType;

    /**
     * @ORM\OneToOne(targetEntity=Stroke::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $stroke;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCylinderSize(): ?int
    {
        return $this->cylinderSize;
    }

    public function setCylinderSize(int $cylinderSize): self
    {
        $this->cylinderSize = $cylinderSize;

        return $this;
    }

    public function getModel(): ?model
    {
        return $this->model;
    }

    public function setModel(model $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getMotoType(): ?MotoType
    {
        return $this->motoType;
    }

    public function setMotoType(MotoType $motoType): self
    {
        $this->motoType = $motoType;

        return $this;
    }

    public function getStroke(): ?Stroke
    {
        return $this->stroke;
    }

    public function setStroke(Stroke $stroke): self
    {
        $this->stroke = $stroke;

        return $this;
    }
}
