<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StrokeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=StrokeRepository::class)
 */
class Stroke
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $stroke;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStroke(): ?int
    {
        return $this->stroke;
    }

    public function setStroke(int $stroke): self
    {
        $this->stroke = $stroke;

        return $this;
    }
}
