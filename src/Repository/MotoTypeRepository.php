<?php

namespace App\Repository;

use App\Entity\MotoType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MotoType|null find($id, $lockMode = null, $lockVersion = null)
 * @method MotoType|null findOneBy(array $criteria, array $orderBy = null)
 * @method MotoType[]    findAll()
 * @method MotoType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotoTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MotoType::class);
    }

    // /**
    //  * @return MotoType[] Returns an array of MotoType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MotoType
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
