<?php

namespace App\Repository;

use App\Entity\Stroke;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stroke|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stroke|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stroke[]    findAll()
 * @method Stroke[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StrokeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stroke::class);
    }

    // /**
    //  * @return Stroke[] Returns an array of Stroke objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Stroke
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
