<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200616163731 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE brand (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE model (id INT AUTO_INCREMENT NOT NULL, brand_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_D79572D944F5D008 (brand_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE moto_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stroke (id INT AUTO_INCREMENT NOT NULL, stroke INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE model ADD CONSTRAINT FK_D79572D944F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id)');
        $this->addSql('ALTER TABLE moto ADD model_id INT NOT NULL, ADD moto_type_id INT NOT NULL, ADD stroke_id INT NOT NULL');
        $this->addSql('ALTER TABLE moto ADD CONSTRAINT FK_3DDDBCE47975B7E7 FOREIGN KEY (model_id) REFERENCES model (id)');
        $this->addSql('ALTER TABLE moto ADD CONSTRAINT FK_3DDDBCE4A65A1067 FOREIGN KEY (moto_type_id) REFERENCES moto_type (id)');
        $this->addSql('ALTER TABLE moto ADD CONSTRAINT FK_3DDDBCE4FE725516 FOREIGN KEY (stroke_id) REFERENCES stroke (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3DDDBCE47975B7E7 ON moto (model_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3DDDBCE4A65A1067 ON moto (moto_type_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3DDDBCE4FE725516 ON moto (stroke_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE model DROP FOREIGN KEY FK_D79572D944F5D008');
        $this->addSql('ALTER TABLE moto DROP FOREIGN KEY FK_3DDDBCE47975B7E7');
        $this->addSql('ALTER TABLE moto DROP FOREIGN KEY FK_3DDDBCE4A65A1067');
        $this->addSql('ALTER TABLE moto DROP FOREIGN KEY FK_3DDDBCE4FE725516');
        $this->addSql('DROP TABLE brand');
        $this->addSql('DROP TABLE model');
        $this->addSql('DROP TABLE moto_type');
        $this->addSql('DROP TABLE stroke');
        $this->addSql('DROP INDEX UNIQ_3DDDBCE47975B7E7 ON moto');
        $this->addSql('DROP INDEX UNIQ_3DDDBCE4A65A1067 ON moto');
        $this->addSql('DROP INDEX UNIQ_3DDDBCE4FE725516 ON moto');
        $this->addSql('ALTER TABLE moto DROP model_id, DROP moto_type_id, DROP stroke_id');
    }
}
